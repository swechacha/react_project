"use strict";
/*
 * ATTENTION: An "eval-source-map" devtool has been used.
 * This devtool is neither made for production nor for readable output files.
 * It uses "eval()" calls to create a separate source file with attached SourceMaps in the browser devtools.
 * If you are trying to read the output file, select a different devtool (https://webpack.js.org/configuration/devtool/)
 * or disable the default devtool with "devtool: false".
 * If you are looking for production-ready output files, see mode: "production" (https://webpack.js.org/configuration/mode/).
 */
(() => {
var exports = {};
exports.id = "pages/index";
exports.ids = ["pages/index"];
exports.modules = {

/***/ "./pages/index.jsx":
/*!*************************!*\
  !*** ./pages/index.jsx ***!
  \*************************/
/***/ ((__unused_webpack_module, __webpack_exports__, __webpack_require__) => {

eval("__webpack_require__.r(__webpack_exports__);\n/* harmony export */ __webpack_require__.d(__webpack_exports__, {\n/* harmony export */   \"default\": () => (/* binding */ HomePage)\n/* harmony export */ });\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__ = __webpack_require__(/*! react/jsx-dev-runtime */ \"react/jsx-dev-runtime\");\n/* harmony import */ var react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0___default = /*#__PURE__*/__webpack_require__.n(react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__);\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1__ = __webpack_require__(/*! react */ \"react\");\n/* harmony import */ var react__WEBPACK_IMPORTED_MODULE_1___default = /*#__PURE__*/__webpack_require__.n(react__WEBPACK_IMPORTED_MODULE_1__);\n\n// index\n\nfunction Header({ title  }) {\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"h1\", {\n        children: title ? title : 'Default title'\n    }, void 0, false, {\n        fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n        lineNumber: 5,\n        columnNumber: 12\n    }, this));\n}\nfunction HomePage() {\n    (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);\n    const { 0: likes , 1: setLikes  } = (0,react__WEBPACK_IMPORTED_MODULE_1__.useState)(0);\n    const names = [\n        'Ada Lovelace',\n        'Grace Hopper',\n        'Margaret Hamilton'\n    ];\n    function handleClick() {\n        console.log(\"increment like count\");\n        setLikes(likes + 1);\n    }\n    return(/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"div\", {\n        children: [\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(Header, {\n                title: \"React <3\"\n            }, void 0, false, {\n                fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n                lineNumber: 22,\n                columnNumber: 13\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"ul\", {\n                children: names.map((name)=>/*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"li\", {\n                        children: name\n                    }, name, false, {\n                        fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n                        lineNumber: 25,\n                        columnNumber: 17\n                    }, this)\n                )\n            }, void 0, false, {\n                fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n                lineNumber: 23,\n                columnNumber: 13\n            }, this),\n            /*#__PURE__*/ (0,react_jsx_dev_runtime__WEBPACK_IMPORTED_MODULE_0__.jsxDEV)(\"button\", {\n                onClick: handleClick,\n                children: [\n                    \"LIKE (\",\n                    likes,\n                    \")\"\n                ]\n            }, void 0, true, {\n                fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n                lineNumber: 28,\n                columnNumber: 13\n            }, this)\n        ]\n    }, void 0, true, {\n        fileName: \"/Users/swechchhaparajuli/Desktop/JobSearch/programs/React/react_project/pages/index.jsx\",\n        lineNumber: 20,\n        columnNumber: 9\n    }, this));\n};\n//# sourceURL=[module]\n//# sourceMappingURL=data:application/json;charset=utf-8;base64,eyJ2ZXJzaW9uIjozLCJmaWxlIjoiLi9wYWdlcy9pbmRleC5qc3guanMiLCJtYXBwaW5ncyI6Ijs7Ozs7Ozs7O0FBQUEsRUFBUTtBQUN3QjtTQUV2QkMsTUFBTSxDQUFDLENBQUNDLENBQUFBLEtBQUssR0FBQyxFQUFFLENBQUM7SUFDdEIsTUFBTSw2RUFBRUMsQ0FBRTtrQkFBRUQsS0FBSyxHQUFHQSxLQUFLLEdBQUcsQ0FBZTs7Ozs7O0FBQy9DLENBQUM7QUFFYyxRQUFRLENBQUNFLFFBQVEsR0FBRyxDQUFDO0lBQzVCSiwrQ0FBUSxDQUFDLENBQUM7SUFDVixLQUFLLE1BQUVLLEtBQUssTUFBRUMsUUFBUSxNQUFJTiwrQ0FBUSxDQUFDLENBQUM7SUFFcEMsS0FBSyxDQUFDTyxLQUFLLEdBQUcsQ0FBQztRQUFBLENBQWM7UUFBRSxDQUFjO1FBQUUsQ0FBbUI7SUFBQSxDQUFDO2FBRTFEQyxXQUFXLEdBQUcsQ0FBQztRQUNwQkMsT0FBTyxDQUFDQyxHQUFHLENBQUMsQ0FBc0I7UUFDbENKLFFBQVEsQ0FBQ0QsS0FBSyxHQUFHLENBQUM7SUFDdEIsQ0FBQztJQUVMLE1BQU0sNkVBQ0RNLENBQUc7O3dGQUVDVixNQUFNO2dCQUFDQyxLQUFLLEVBQUUsQ0FBVTs7Ozs7O3dGQUN4QlUsQ0FBRTswQkFDRUwsS0FBSyxDQUFDTSxHQUFHLEVBQUNDLElBQUksK0VBQ2RDLENBQUU7a0NBQWFELElBQUk7dUJBQVhBLElBQUk7Ozs7Ozs7Ozs7O3dGQUdoQkUsQ0FBTTtnQkFBQ0MsT0FBTyxFQUFFVCxXQUFXOztvQkFBRSxDQUFNO29CQUFDSCxLQUFLO29CQUFDLENBQUM7Ozs7Ozs7Ozs7Ozs7QUFHeEQsQ0FBQyIsInNvdXJjZXMiOlsid2VicGFjazovLy8uL3BhZ2VzL2luZGV4LmpzeD83ZmZkIl0sInNvdXJjZXNDb250ZW50IjpbIi8vIGluZGV4XG5pbXBvcnQgeyB1c2VTdGF0ZSB9IGZyb20gJ3JlYWN0J1xuXG5mdW5jdGlvbiBIZWFkZXIoe3RpdGxlfSkge1xuICAgIHJldHVybiA8aDE+e3RpdGxlID8gdGl0bGUgOiAnRGVmYXVsdCB0aXRsZSd9PC9oMT5cbn1cblxuZXhwb3J0IGRlZmF1bHQgZnVuY3Rpb24gSG9tZVBhZ2UoKSB7XG4gICAgICAgIHVzZVN0YXRlKDApXG4gICAgICAgIGNvbnN0IFtsaWtlcywgc2V0TGlrZXNdID0gdXNlU3RhdGUoMClcblxuICAgICAgICBjb25zdCBuYW1lcyA9IFsnQWRhIExvdmVsYWNlJywgJ0dyYWNlIEhvcHBlcicsICdNYXJnYXJldCBIYW1pbHRvbiddXG5cbiAgICAgICAgZnVuY3Rpb24gaGFuZGxlQ2xpY2soKSB7XG4gICAgICAgICAgICBjb25zb2xlLmxvZyhcImluY3JlbWVudCBsaWtlIGNvdW50XCIpXG4gICAgICAgICAgICBzZXRMaWtlcyhsaWtlcyArIDEpXG4gICAgICAgIH1cblxuICAgIHJldHVybiAoXG4gICAgICAgIDxkaXY+XG4gICAgICAgICAgICAgICAgICAgIHsvKiBOZXN0aW5nIHRoZSBIZWFkZXIgY29tcG9uZW50ICovfVxuICAgICAgICAgICAgPEhlYWRlciB0aXRsZT0gXCJSZWFjdCA8M1wiLz5cbiAgICAgICAgICAgIDx1bD5cbiAgICAgICAgICAgICAgICB7bmFtZXMubWFwKG5hbWUgPT4gKFxuICAgICAgICAgICAgICAgIDxsaSBrZXk9e25hbWV9PntuYW1lfTwvbGk+XG4gICAgICAgICAgICAgICAgKSl9XG4gICAgICAgICAgICA8L3VsPiBcbiAgICAgICAgICAgIDxidXR0b24gb25DbGljaz17aGFuZGxlQ2xpY2t9PkxJS0UgKHtsaWtlc30pPC9idXR0b24+ICBcbiAgICAgICAgPC9kaXY+XG4gICAgICAgIClcbn1cbiJdLCJuYW1lcyI6WyJ1c2VTdGF0ZSIsIkhlYWRlciIsInRpdGxlIiwiaDEiLCJIb21lUGFnZSIsImxpa2VzIiwic2V0TGlrZXMiLCJuYW1lcyIsImhhbmRsZUNsaWNrIiwiY29uc29sZSIsImxvZyIsImRpdiIsInVsIiwibWFwIiwibmFtZSIsImxpIiwiYnV0dG9uIiwib25DbGljayJdLCJzb3VyY2VSb290IjoiIn0=\n//# sourceURL=webpack-internal:///./pages/index.jsx\n");

/***/ }),

/***/ "react":
/*!************************!*\
  !*** external "react" ***!
  \************************/
/***/ ((module) => {

module.exports = require("react");

/***/ }),

/***/ "react/jsx-dev-runtime":
/*!****************************************!*\
  !*** external "react/jsx-dev-runtime" ***!
  \****************************************/
/***/ ((module) => {

module.exports = require("react/jsx-dev-runtime");

/***/ })

};
;

// load runtime
var __webpack_require__ = require("../webpack-runtime.js");
__webpack_require__.C(exports);
var __webpack_exec__ = (moduleId) => (__webpack_require__(__webpack_require__.s = moduleId))
var __webpack_exports__ = (__webpack_exec__("./pages/index.jsx"));
module.exports = __webpack_exports__;

})();
// index
import { useState } from 'react'

function Header({title}) {
    return <h1>{title ? title : 'Default title'}</h1>
}

export default function HomePage() {
        useState(0)
        const [likes, setLikes] = useState(0)

        const names = ['Ada Lovelace', 'Grace Hopper', 'Margaret Hamilton']

        function handleClick() {
            console.log("increment like count")
            setLikes(likes + 1)
        }

    return (
        <div>
                    {/* Nesting the Header component */}
            <Header title= "React <3"/>
            <ul>
                {names.map(name => (
                <li key={name}>{name}</li>
                ))}
            </ul> 
            <button onClick={handleClick}>LIKE ({likes})</button>  
        </div>
        )
}
